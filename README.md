## `composer require infotechnohelp/cakephp-remove-contents`

`bin/cake plugin load RemoveContents`

## Usage

`bin/cake remove_contents <cinfug-yaml-path>`

### I.E.

`bin/cake remove_contents Bakery/CakePhpBakedFiles/model.yml`

Uses `APP/config/Bakery/CakePhpBakedFiles/model.yml`