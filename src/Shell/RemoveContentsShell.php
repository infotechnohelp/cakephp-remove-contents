<?php

namespace RemoveContents\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Symfony\Component\Yaml\Yaml;

/**
 * Class RemoveContentsShell
 * @package RemoveContents\Shell
 */
class RemoveContentsShell extends Shell
{
    public function main(string $configPath)
    {
        $this->out('REMOVED:');

        $failed = [];

        $config = Yaml::parse(file_get_contents(CONFIG . $configPath));

        foreach ($config as $filePath) {
            $File = null;
            $Folder = null;

            if (strpos($filePath, '/') === false) {
                $File = new File(ROOT . DS . $filePath);
            } else {
                if (substr($filePath, -1) === '/') {
                    $Folder = new Folder($filePath);
                } else {
                    $File = new File($filePath);
                }
            }

            if ($File !== null) {
                if ($File->delete()) {
                    $this->out($File->path);
                } else {
                    $failed[] = $filePath;
                }
            } else {
                if ($Folder->delete()) {
                    $this->out($Folder->path);
                } else {
                    $failed[] = $filePath;
                }
            }
        }

        $this->out('NOT REMOVED:');

        foreach ($failed as $filePath) {
            $this->out($filePath);
        }
    }
}
